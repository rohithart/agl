(function () {
    'use strict';

    angular.module('agl').controller('PersonCtrl', personController);
    personController.$inject = ['personService'];
    function personController(personService) {
        var personVM = this;
        personVM.persons = '';
        personVM.person = '';
        personVM.searchText = '';


        //get All persons
        personVM.getPersons = function () {
            personService.getPersons().then(function (response) {
                personVM.persons = response;
            });
        };

        //get All persons grouped by gender
        personVM.getGroupedPersons = function () {
            personService.getGroupedPersons().then(function (response) {
                personVM.groupedPersons = response;
            });
        };

        personVM.init = function () {
            personVM.getPersons();
            personVM.editFlag = false;
            personVM.editTabFlag = false;
        };
        personVM.init();

    };

})();
