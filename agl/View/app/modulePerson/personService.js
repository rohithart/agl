/**
 * Created by Administrator on 04/12/2015.
 */
(function () {
    'use strict';
    angular.module('agl').service('personService', personService);
    personService.$inject = ['$q', '$http'];
    function personService($q, $http) {
        var self = this;
        if (sessionStorage.getItem('Authorization')) {
            $http.defaults.headers.common['Authorization'] = sessionStorage.getItem('Authorization');
            $http.defaults.headers.common['user'] = sessionStorage.getItem('email');
        }
        self.getPersons = function (){
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: '/api/Person'

            })
                .success(function (data) {
                    defer.resolve(data);
                })
                .error(function (err) {
                    defer.reject(err)
                })
            ;
            return defer.promise;
        };

        self.getGroupedPersons = function () {
            var defer = $q.defer();
            $http({
                method: 'PUT',
                url: '/api/Person/',

            })
                .success(function (data) {
                    defer.resolve(data);
                })
                .error(function (err) {
                    defer.reject(err)
                })
            ;
            return defer.promise;
        };

    };

})();
