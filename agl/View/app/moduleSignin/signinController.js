(function () {
    'use strict';

    angular.module('agl').controller('SigninCtrl', signinController);
    signinController.$inject = ['$scope',  'signinService'];
    function signinController( $scope, signinService) {
        var signinVM = this;
        signinVM.userLoginData = { email: '', password: '', repeatPassword: '' };

        signinVM.init = function () {

        };
        signinVM.init();
    };

})();
