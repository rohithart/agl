﻿using agl.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace agl.HelperClass
{
    public class ServerHelper
    {
        private static string url = "http://agl-developer-test.azurewebsites.net/people.json";
        public static IEnumerable<Person> getAllPersonsFromServer()
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage response = null;
            response = client.GetAsync(url).Result;
            if (response.IsSuccessStatusCode) //if success
            {
                //get request result
                var responseValue = response.Content.ReadAsStringAsync().Result;
                //InvalidCastException result to person object
                IEnumerable<Person> list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Person>>(responseValue);
                return list;
            }
            //if not success return null
            return null;
        }
    }
}