﻿using agl.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace agl.HelperClass
{

    public class PersonHelper
    {
        //get all persons and the group it by owners gender and order by pets name
        public IEnumerable<IGrouping<string, PetPerson>> getAllPersonsGrouped()
        {
            List<Person> personList = getFromServer().ToList();
            var petList = (from person in personList
                           where person.Pets != null
                           from pets in (from catPets in person.Pets
                                         where catPets.type.ToLower().Equals("cat")
                                         select catPets)
                           orderby pets.name
                           select new PetPerson { personGender = person.gender, pet = pets })
                           .GroupBy(u => u.personGender).ToList();

            return petList;

        }

        //get all persons and the group it by owners gender and order by pets name
        //public IEnumerable<IGrouping<string, PetPerson>> getAllPersonsGrouped()
        //{
        //    IEnumerable<Person> personList = getFromServer();
        //    personList = personList.ToList().Where(person => person.Pets != null);
        //    List<PetPerson> petPersonList = new List<PetPerson>();

        //    foreach (var person in personList)
        //    {
        //        person.Pets = person.Pets.Where(pet => pet.type.ToLower() == "cat").ToList();
        //        foreach (var pet in person.Pets)
        //        {
        //            petPersonList.Add(new PetPerson { personGender = person.gender, pet = pet });
        //        }
        //    }


        //    IEnumerable<IGrouping<string, PetPerson>> plIst =petPersonList.OrderBy(pet => pet.pet.name).GroupBy(p=>p.personGender).ToList();

        //    return plIst;
        //}


        //get All persons
        public IEnumerable<Person> getAllPersons()
        {
            return getFromServer();
        }


        //get All persons from server
        private IEnumerable<Person> getFromServer()
        {
            return ServerHelper.getAllPersonsFromServer();
        }
    }
}