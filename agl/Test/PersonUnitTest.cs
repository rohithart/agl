﻿using agl.HelperClass;
using agl.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using Webjet.Controllers;

namespace agl.Test
{
    [TestClass]
    public class PersonUnitTest
    {
        [TestMethod]
        public void getAllPersonsTest()
        {
            // Set up Prerequisites   
            var controller = new PersonController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            // Act on Test  
            var response = controller.Get();
            var contentResult = response as OkNegotiatedContentResult <IEnumerable<Person>>;
            // Assert the result  
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);

        }

        [TestMethod]
        public void getAllPersonsGroupedTest()
        {
            // Set up Prerequisites   
            var controller = new PersonController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            // Act on Test  
            var response = controller.Put();
            var contentResult = response as OkNegotiatedContentResult<IEnumerable<IGrouping<string, PetPerson>>>;
            // Assert the result  
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);

        }
    }
}