﻿using agl.HelperClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Webjet.Controllers
{
    public class PersonController : ApiController
    {
        // GET: api/Person

        /*
            Get All persons from server 
        */
        public IHttpActionResult Get()
        {
            PersonHelper helper = new PersonHelper();
            return Ok(helper.getAllPersons());
        }
        // PUT: api/Person

        /*
        Get All persons from server and the group by owener's gender
        */
        public IHttpActionResult Put()
        {
            PersonHelper helper = new PersonHelper();
            return Ok(helper.getAllPersonsGrouped());
        }
    }
}
